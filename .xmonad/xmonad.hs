    -- Base
import XMonad
import System.Directory
import System.IO (hPutStrLn)
import qualified XMonad.StackSet as W

    -- Actions
import XMonad.Actions.CopyWindow (kill1, killAllOtherCopies)
-- import XMonad.Actions.DynamicWorkspaceOrder (Next, Prev)
import XMonad.Actions.CycleWS (moveTo, shiftTo, WSType(..), nextScreen, prevScreen, Direction1D(Next, Prev))
import XMonad.Actions.MouseResize
import XMonad.Actions.RotSlaves (rotSlavesDown, rotAllDown)
import XMonad.Actions.WithAll (sinkAll, killAll)

    -- Data
import Data.Maybe (isJust)

    -- Hooks
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.FadeInactive
import XMonad.Hooks.ManageDocks (avoidStruts, docksEventHook, manageDocks, ToggleStruts(..))
import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat)
import XMonad.Hooks.ServerMode
import XMonad.Hooks.WorkspaceHistory

    -- Layouts
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spiral
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns

    -- Layouts modifiers
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.Magnifier
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed
import XMonad.Layout.ShowWName
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import XMonad.Layout.WindowNavigation
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))

    -- Utilities
import XMonad.Util.Cursor
import XMonad.Util.EZConfig (additionalKeysP)
-- import XMonad.Util.NamedScratchpad
import XMonad.Util.Run (runProcessWithInput, safeSpawn, spawnPipe)
import XMonad.Util.SpawnOnce

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

myFont :: String
myFont = "xft:SauceCodePro Nerd Font Mono:regular:size=9:antialias=true:hinting=true"

myBar :: String
myBar = "xmobar"

myPP = xmobarPP { ppCurrent = xmobarColor "#ffeb84" "" . wrap "<" ">" }

myTerminal :: String
myTerminal    = "kitty"

myNormalColor :: String
myNormalColor = "#282c34"

myFocusColor :: String
myFocusColor = "#fce366"

myModMask :: KeyMask
myModMask = mod4Mask

myBorderWidth :: Dimension
myBorderWidth = 2

myLogHook :: X ()
myLogHook = fadeInactiveLogHook fadeAmount
    where fadeAmount = 0.98

mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

myWorkspaces = [" dev <fn=1>\xf296 </fn>", " www <fn=1>\xf0ac </fn>", " sys <fn=1>\xf17c </fn>", " doc <fn=1>\xf0f6 </fn>", " mail <fn=1>\xf003 </fn>", " vbox <fn=1>\xf2d2 </fn>", " mus <fn=1>\xf001 </fn>", " vid <fn=1>\xf16a </fn> ", " gfx <fn=1>\xf030 </fn>"]

xmobarEscape :: String -> String
xmobarEscape = concatMap doubleLts
  where
        doubleLts '<' = "<<"
        doubleLts x   = [x]


myClickableWorkspaces :: [String]
myClickableWorkspaces = clickable . (map xmobarEscape)
               -- $ [" 1 ", " 2 ", " 3 ", " 4 ", " 5 ", " 6 ", " 7 ", " 8 ", " 9 "]
               $ [" dev ", " www ", " sys ", " doc ", " mail ", " vbox ", " mus ", " vid ", " gfx "] 
--               $ [" dev <fn=1>\xf296 </fn>", " www <fn=1>\xf0ac </fn>", " sys <fn=1>\xf17c </fn>", " doc <fn=1>\xf0f6 </fn>", " mail <fn=1>\xf003 </fn>", " vbox <fn=1>\xf2d2 </fn>", " mus <fn=1>\xf001 </fn>", " vid <fn=1>\xf16a </fn> ", " gfx <fn=1>\xf030 </fn>"] 
  where
        clickable l = [ "<action=xdotool key super+" ++ show (n) ++ ">" ++ ws ++ "</action>" |
                      (i,ws) <- zip [1..9] l,
                      let n = i ]


myKeys :: String -> [([Char], X ())]
myKeys home =
    -- Windows navigation
        -- , ("M-m", windows W.focusMaster)  -- Move focus to the master window
        [ ("M-S-k", windows W.focusDown)    -- Move focus to the next window
        , ("M-S-j", windows W.focusUp)      -- Move focus to the prev window
        , ("M-S-m", windows W.swapMaster) -- Swap the focused window and the master window
        , ("M-S-f", windows W.swapDown)   -- Swap focused window with next window
        , ("M-S-b", windows W.swapUp)     -- Swap focused window with prev window
        -- , ("M-<Backspace>", promote)      -- Moves focused window to master, others maintain order
        -- , ("M-S-<Tab>", rotSlavesDown)    -- Rotate all windows except master and keep focus in place
        , ("M-S-<Tab>", rotAllDown)       -- Rotate all the windows in the current stack

    -- Workspaces
        -- , ("M-S-<Right>", nextScreen)  -- Switch focus to next monitor
        -- , ("M-S-<Left>", prevScreen)  -- Switch focus to prev monitor
        , ("M-S-<Right>", shiftTo Next nonNSP >> moveTo Next nonNSP) -- Shifts focused window to next ws
        , ("M-S-<Left>", shiftTo Prev nonNSP >> moveTo Prev nonNSP)  -- Shifts focused window to prev ws

    -- Layouts
        , ("M-S-l", sendMessage NextLayout)           -- Switch to next layout
        -- , ("M-C-M1-<Up>", sendMessage Arrange)
        -- , ("M-C-M1-<Down>", sendMessage DeArrange)
        -- , ("M-<Space>", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts) -- Toggles noborder/full
        -- , ("M-S-<Space>", sendMessage ToggleStruts)     -- Toggles struts
        -- , ("M-S-n", sendMessage $ MT.Toggle NOBORDERS)  -- Toggles noborder


    -- Window resizing
        , ("M-S-s", sendMessage Shrink)                  -- Shrink horiz window width
        , ("M-S-e", sendMessage Expand)                  -- Expand horiz window width
        -- , ("M-S-h", sendMessage MirrorShrink)          -- Shrink vert window width (Not working)
        -- , ("M-S-l", sendMessage MirrorExpand)          -- Expand vert window width (Not working)

    -- Kill windows
        , ("M-S-c", kill1)     -- Kill the currently focused client
        , ("M-S-a", killAll)   -- Kill all windows on current workspace
        ]


    -- The following lines are needed for named scratchpads.
          where nonNSP          = WSIs (return (\ws -> W.tag ws /= "nsp"))
                nonEmptyNonNSP  = WSIs (return (\ws -> isJust (W.stack ws) && W.tag ws /= "nsp"))

myStartupHook :: X ()
myStartupHook = do
        spawnOnce "lxsession &"
        spawnOnce "nitrogen --restore &"
        spawnOnce "picom --experimental-backends &"
--        spawnOnce "nm-applet &"
--        spawnOnce "volumeicon &"
--        spawnOnce "trayer --edge top --align center --widthtype request --iconspacing 3 --SetDockType true --SetPartialStrut true --expand true --monitor 0 --transparent true --alpha 0 --tint 0x282c34  --height 22 &"
        spawnOnce "setxkbmap es"
        setDefaultCursor xC_left_ptr

main :: IO ()
main = do
    home <- getHomeDirectory
    -- Launching three instances of xmobar on their monitors.
    xmproc0 <- spawnPipe "xmobar -x 0 $HOME/.config/xmobar/xmobarrc"
    xmonad $ ewmh def
        { manageHook = ( isFullscreen --> doFullFloat ) <+> manageDocks
        -- Run xmonad commands from command line with "xmonadctl command". Commands include:
        -- shrink, expand, next-layout, default-layout, restart-wm, xterm, kill, refresh, run,
        -- focus-up, focus-down, swap-up, swap-down, swap-master, sink, quit-wm. You can run
        -- "xmonadctl 0" to generate full list of commands written to ~/.xsession-errors.
        -- To compile xmonadctl: ghc -dynamic xmonadctl.hs
        , handleEventHook    = serverModeEventHookCmd
                               <+> serverModeEventHook
                               <+> serverModeEventHookF "XMONAD_PRINT" (io . putStrLn)
                               <+> docksEventHook
        , modMask            = myModMask
        , terminal           = myTerminal
        , startupHook        = myStartupHook
        , layoutHook         = avoidStruts $ mySpacing 3 $ layoutHook def
        -- , layoutHook         = showWName' myShowWNameTheme $ myLayoutHook
        , workspaces         = myWorkspaces
        , borderWidth        = myBorderWidth
        , normalBorderColor  = myNormalColor
        , focusedBorderColor = myFocusColor
        , logHook = workspaceHistoryHook <+> myLogHook <+> dynamicLogWithPP xmobarPP
                        { ppOutput = \x -> hPutStrLn xmproc0 x
                        , ppCurrent = xmobarColor "#98be65" "" . wrap "[" "]" -- Current workspace in xmobar
                        , ppVisible = xmobarColor "#98be65" ""                -- Visible but not current workspace
                        , ppHidden = xmobarColor "#82AAFF" "" . wrap "*" ""   -- Hidden workspaces in xmobar
                        , ppHiddenNoWindows = xmobarColor "#b3afc2" ""        -- Hidden workspaces (no windows) purple (#c792ea)
                        , ppTitle = xmobarColor "#eae992" "" . shorten 60     -- Title of active window in xmobar
                        , ppSep =  "<fc=#666666> <fn=1><-></fn> </fc>"        -- Separators in xmobar
                        , ppUrgent = xmobarColor "#C45500" "" . wrap "!" "!"  -- Urgent workspace
                        , ppExtras  = [windowCount]                           -- # of windows current workspace
                        , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]
                        }
        } `additionalKeysP` myKeys home
